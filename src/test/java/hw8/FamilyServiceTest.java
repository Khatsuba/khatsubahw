package hw8;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class FamilyServiceTest {

    @Test
    public void getAllFamilies() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        expected.add(family2);
        assertEquals(expected, fs.getAllFamilies());
    }

    @Test
    public void displayAllFamilies() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        String expected =
                "Family 1\n" +
                "father Human{name='Sergei', surname='Ignishenko', year=1960, iq=90, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "mother Human{name='Oksana', surname='Ignishenko', year=1962, iq=130, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "children:\n" +
                "Human{name='Petro', surname='Ignishenko', year=2001, iq=100, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "pet Dog{nickname='Charlie', age=7, trickLevel=30, habits=[habit1, habit2]}\n" +
                "\n" +
                "Family 2\n" +
                "father Human{name='Mihailo', surname='Kolechi', year=1960, iq=90, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "mother Human{name='Svetlana', surname='Kolechi', year=1962, iq=130, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "children:\n" +
                "Human{name='Olena', surname='Kolechi', year=1993, iq=110, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "\n";
        assertEquals(expected, fs.displayAllFamilies());
    }

    @Test
    public void getFamiliesBiggerThan1() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        expected.add(family2);
        assertEquals(expected, fs.getFamiliesBiggerThan(2));
    }

    @Test
    public void getFamiliesBiggerThan2() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        assertEquals(expected, fs.getFamiliesBiggerThan(2));
    }

    @Test
    public void getFamiliesBiggerThan3() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        assertEquals(expected, fs.getFamiliesBiggerThan(3));
    }

    @Test
    public void getFamiliesLessThan1() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        expected.add(family2);
        assertEquals(expected, fs.getFamiliesLessThan(4));
    }

    @Test
    public void getFamiliesLessThan2() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        assertEquals(expected, fs.getFamiliesLessThan(3));
    }

    @Test
    public void getFamiliesLessThan3() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        List<Family> expected = new ArrayList<>();
        assertEquals(expected, fs.getFamiliesLessThan(3));
    }
    @Test
    public void countFamiliesWithMemberNumber1() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

            Set<String> habits = new HashSet<>();
            habits.add("habit1");
            habits.add("habit2");

            Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
            Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
            Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
            Pet pet1 = new Dog("Charlie", 7, 30, habits);

            Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
            Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
            Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

            Family family1 = new Family(father1,mother1);
            Family family2 = new Family(father2,mother2);
            family1.addPet(pet1);
            family1.addChild(child1);
            family2.addChild(child2);

            FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
            dao.saveFamily(family1);
            dao.saveFamily(family2);
            FamilyService fs = new FamilyService(dao);

            int expected = 2;
            assertEquals(expected, fs.countFamiliesWithMemberNumber(3));
    }
    @Test
    public void countFamiliesWithMemberNumber2() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        int expected = 1;
        assertEquals(expected, fs.countFamiliesWithMemberNumber(3));
    }
    @Test
    public void countFamiliesWithMemberNumber3() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        int expected = 0;
        assertEquals(expected, fs.countFamiliesWithMemberNumber(4));
    }

    @Test
    public void createNewFamily1() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");}

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        FamilyService fs = new FamilyService(dao);
        fs.createNewFamily(father1, mother1);

        ArrayList<Family> expected = new ArrayList<>();
        expected.add(new Family(father1, mother1));
        assertEquals(expected, dao.getAllFamilies());
    }

    @Test
    public void createNewFamily2() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Family family1 = new Family(father1, mother1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        FamilyService fs = new FamilyService(dao);
        dao.saveFamily(family1); // така сім'я вже існує
        fs.createNewFamily(father1, mother1);

        ArrayList<Family> expected = new ArrayList<>();
        expected.add(family1); //лише одна сім'я, дублікатів немає
        assertEquals(expected, dao.getAllFamilies());
    }

    @Test
    public void deleteFamilyByIndex() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        fs.deleteFamilyByIndex(2); //видалення другої сім'ї
        ArrayList<Family> expected = new ArrayList<>();
        expected.add(family1);
        assertEquals(expected, dao.getAllFamilies());
    }

    @Test
    public void bornChild() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Family family1 = new Family(father1,mother1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        fs.bornChild(family1, "boy", "girl");
        int expected = 1;
        assertEquals(expected, family1.getChildren().size());
    }

    @Test
    public void adoptChild() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);

        Family family1 = new Family(father1,mother1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        fs.adoptChild(family1, child1);
        int expected = 1;
        assertEquals(expected, family1.getChildren().size());
    }

    @Test
    public void deleteAllChildrenOlderThan1() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);

        Family family1 = new Family(father1,mother1);
        family1.addChild(child1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        fs.deleteAllChildrenOlderThan(2);
        int expected = 0;
        assertEquals(expected, family1.getChildren().size());
    }
    @Test
    public void deleteAllChildrenOlderThan2() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);

        Family family1 = new Family(father1,mother1);
        family1.addChild(child1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        fs.deleteAllChildrenOlderThan(50);
        int expected = 1;
        assertEquals(expected, family1.getChildren().size());
    }

    @Test
    public void count() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        int expected = 2;
        assertEquals(expected, fs.count());
    }

    @Test
    public void getFamilyById() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Man father2 = new Man("Mihailo", "Kolechi", 1960, 90, schedule);
        Woman mother2 = new Woman("Svetlana", "Kolechi", 1962, 130, schedule);
        Woman child2 = new Woman("Olena", "Kolechi", 1993, 110, schedule);

        Family family1 = new Family(father1,mother1);
        Family family2 = new Family(father2,mother2);
        family1.addPet(pet1);
        family1.addChild(child1);
        family2.addChild(child2);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService fs = new FamilyService(dao);

        Family expected = family1;
        assertEquals(expected, fs.getFamilyById(1));
    }

    @Test
    public void getPets() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family1 = new Family(father1,mother1);
        family1.addPet(pet1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        HashSet<Pet> expected = new HashSet<>();
        expected.add(pet1);
        assertEquals(expected, fs.getPets(1));
    }

    @Test
    public void addPet() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family1 = new Family(father1,mother1);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        fs.addPet(1, pet1);
        HashSet<Pet> expected = new HashSet<>();
        expected.add(pet1);
        assertEquals(expected, fs.getPets(1));
    }
}