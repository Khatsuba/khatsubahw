package hw9;

import hw9.CollectionFamilyDAO;
import hw9.DayOfWeek;
import hw9.Family;
import hw9.FamilyDAO;
import hw9.FamilyService;
import hw9.Man;
import hw9.Woman;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

public class FamilyServiceTest {

    @Test
    public void deleteAllChildrenOlderThan2() {
        Map<hw9.DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(hw9.DayOfWeek.Monday, "task1");
            schedule.put(hw9.DayOfWeek.Tuesday, "task2");
            schedule.put(hw9.DayOfWeek.Wednesday, "task3");
            schedule.put(hw9.DayOfWeek.Thursday, "task4");
            schedule.put(hw9.DayOfWeek.Friday, "task5");
            schedule.put(hw9.DayOfWeek.Saturday, "task6");
            schedule.put(hw9.DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        hw9.Man father1 = new hw9.Man("Sergei", "Ignishenko", LocalDate.of(1960,1,1), 90, schedule);
        hw9.Woman mother1 = new hw9.Woman("Oksana", "Ignishenko", LocalDate.of(1962,1,1), 130, schedule);
        hw9.Man child1 = new hw9.Man("Petro", "Ignishenko", LocalDate.of(2002,1,1), 100, schedule);

        hw9.Family family1 = new hw9.Family(father1,mother1);
        family1.addChild(child1);

        hw9.FamilyDAO dao = new hw9.CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        hw9.FamilyService fs = new hw9.FamilyService(dao);

        fs.deleteAllChildrenOlderThan(22);
        int expected = 1;
        assertEquals(expected, family1.getChildren().size());
    }
}