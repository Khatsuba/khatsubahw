package hw7;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class FamilyTest {

    @Test
    public void testToString() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Woman child2 = new Woman("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.addPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        String expected = "father Human{name='Sergei', surname='Ignishenko', year=1960, iq=90, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "mother Human{name='Oksana', surname='Ignishenko', year=1962, iq=130, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "children:\n" +
                "Human{name='Petro', surname='Ignishenko', year=2001, iq=100, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "Human{name='Maria', surname='Ignishenko', year=1993, iq=110, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "pet Dog{nickname='Charlie', age=7, trickLevel=30, habits=[habit1, habit2]}\n";
        assertEquals(expected, family.toString());
    }

    @Test
    public void addChild() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);

        Family family = new Family(father, mother);

        //додаємо дитину до сім'ї без дітей
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        family.addChild(child1);

        List<Human> expected = new ArrayList<>();
        expected.add(child1);

        assertEquals(expected, family.getChildren());
    }

    @Test
    public void deleteChild1() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Woman child2 = new Woman("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.addPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо першу дитину
        family.deleteChild(1);

        List<Human> expected = new ArrayList<>();
        expected.add(child2);

        assertEquals(expected, family.getChildren());
    }

    @Test
    public void DeleteChild2() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Woman child2 = new Woman("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.addPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо дитину з неіснуючим індексом
        family.deleteChild(5);

        List<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);

        assertEquals(expected, family.getChildren());

    }

    @Test
    public void deleteChild3() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Woman child2 = new Woman("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.addPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо дитину яка була додана до сім'ї
        family.deleteChild(child2);

        List<Human> expected = new ArrayList<>();
        expected.add(child1);

        assertEquals(expected, family.getChildren());
    }

    @Test
    public void DeleteChild4() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Woman child2 = new Woman("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.addPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо дитину яка не належить до сім'ї
        Human child3 = new Human("Oleg", "Kornew", 1997, 102, schedule);
        family.deleteChild(child3);

        List<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);

        assertEquals(expected, family.getChildren());

    }

    @Test
    public void countFamily() {
        Map<DayOfWeek, String> schedule = new HashMap();
        schedule.put(DayOfWeek.Monday, "task1");
        schedule.put(DayOfWeek.Tuesday, "task2");
        schedule.put(DayOfWeek.Wednesday, "task3");
        schedule.put(DayOfWeek.Thursday, "task4");
        schedule.put(DayOfWeek.Friday, "task5");
        schedule.put(DayOfWeek.Saturday, "task6");
        schedule.put(DayOfWeek.Sunday, "task7");

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father = new Man("Sergei", "Ignishenko", 1960, 90, schedule);
        Woman mother = new Woman("Oksana", "Ignishenko", 1962, 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", 2001, 100, schedule);
        Woman child2 = new Woman("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Dog("Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.addPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        int expected = 4;
        assertEquals(expected, family.countFamily());
    }
}