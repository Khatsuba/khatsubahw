package hw10;

import hw10.CollectionFamilyDAO;
import hw10.DayOfWeek;
import hw10.Family;
import hw10.FamilyDAO;
import hw10.FamilyService;
import hw10.Man;
import hw10.Woman;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

public class FamilyServiceTest {

    @Test
    public void deleteAllChildrenOlderThan1() {
        Map<DayOfWeek, String> schedule = new HashMap<>();{
            schedule.put(DayOfWeek.Monday, "task1");
            schedule.put(DayOfWeek.Tuesday, "task2");
            schedule.put(DayOfWeek.Wednesday, "task3");
            schedule.put(DayOfWeek.Thursday, "task4");
            schedule.put(DayOfWeek.Friday, "task5");
            schedule.put(DayOfWeek.Saturday, "task6");
            schedule.put(DayOfWeek.Sunday, "task7");}

        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");

        Man father1 = new Man("Sergei", "Ignishenko", LocalDate.of(1960,1,1), 90, schedule);
        Woman mother1 = new Woman("Oksana", "Ignishenko", LocalDate.of(1962,1,1), 130, schedule);
        Man child1 = new Man("Petro", "Ignishenko", LocalDate.of(2000,10,20), 100, schedule);     //to be deleted
        Man child2 = new Man("Petro", "Ignishenko", LocalDate.of(2002,10,20), 100, schedule);
        Man child3 = new Man("Petro", "Ignishenko", LocalDate.of(2004,10,20), 100, schedule);

        Family family1 = new Family(father1,mother1);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);

        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        FamilyService fs = new FamilyService(dao);

        fs.deleteAllChildrenOlderThan(22); //born before 2001
        int expected = 2;
        assertEquals(expected, family1.getChildren().size());
    }
}