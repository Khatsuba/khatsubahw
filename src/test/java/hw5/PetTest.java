package hw5;

import static org.junit.Assert.*;

public class PetTest {

    @org.junit.Test
    public void testToString() {
        String[] habits = {"habit1","habit2"};
        Pet pet = new Pet(EnumSpecies.Parrot, "NAME", 5, 50, habits);
        String expected = "Parrot{nickname='NAME', age=5, trickLevel=50, habits=[habit1, habit2]}";
        assertEquals(expected, pet.toString());
    }
}