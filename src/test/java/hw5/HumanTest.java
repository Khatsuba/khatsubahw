package hw5;

import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void testToString() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        Human human = new Human("Name", "Surname", 2000, 100, schedule);
        String expected = "Human{name='Name', surname='Surname', year=2000, iq=100, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}";
        assertEquals(expected, human.toString());
    }
}