package hw5;

import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTest {

    @Test
    public void testToString() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        Human child2 = new Human("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Pet(EnumSpecies.Dog, "Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.setPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        String expected = "father Human{name='Sergei', surname='Ignishenko', year=1960, iq=90, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "mother Human{name='Oksana', surname='Ignishenko', year=1962, iq=130, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "children:\n" +
                "Human{name='Petro', surname='Ignishenko', year=2001, iq=100, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "Human{name='Maria', surname='Ignishenko', year=1993, iq=110, schedule=[ [ Monday,task1 ] [ Tuesday,task2 ] [ Wednesday,task3 ] [ Thursday,task4 ] [ Friday,task5 ] [ Saturday,task6 ] [ Sunday,task7 ] ]}\n" +
                "pet Dog{nickname='Charlie', age=7, trickLevel=30, habits=[habit1, habit2]}\n";
        assertEquals(expected, family.toString());
    }

    @Test
    public void addChild() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Family family = new Family(father, mother);

        //додаємо дитину до сім'ї без дітей
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        family.addChild(child1);

        Human[] expected = {child1};
        assertEquals(expected, family.getChildren());
    }

    @Test
    public void deleteChild1() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        Human child2 = new Human("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Pet(EnumSpecies.Dog, "Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.setPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо першу дитину
        family.deleteChild(1);

        Human[] expected = {child2};
        assertEquals(expected, family.getChildren());
    }

    @Test
    public void testDeleteChild2() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        Human child2 = new Human("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Pet(EnumSpecies.Dog, "Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.setPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо дитину з неіснуючим індексом
        family.deleteChild(5);

        Human[] expected = {child1, child2};
        assertEquals(expected, family.getChildren());

    }

    @Test
    public void deleteChild3() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        Human child2 = new Human("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Pet(EnumSpecies.Dog, "Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.setPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо дитину яка була додана до сім'ї
        family.deleteChild(child2);

        Human[] expected = {child1};
        assertEquals(expected, family.getChildren());
    }

    @Test
    public void testDeleteChild4() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        Human child2 = new Human("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Pet(EnumSpecies.Dog, "Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.setPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        //видаляємо дитину яка не належить до сім'ї
        Human child3 = new Human("Oleg", "Kornew", 1997, 102, schedule);
        family.deleteChild(child3);

        Human[] expected = {child1, child2};
        assertEquals(expected, family.getChildren());

    }

    @Test
    public void countFamily() {
        String[][] schedule = {
                {DayOfWeek.Monday.name(),"task1"},
                {DayOfWeek.Tuesday.name(),"task2"},
                {DayOfWeek.Wednesday.name(),"task3"},
                {DayOfWeek.Thursday.name(),"task4"},
                {DayOfWeek.Friday.name(),"task5"},
                {DayOfWeek.Saturday.name(),"task6"},
                {DayOfWeek.Sunday.name(),"task7"},
        };
        String[] habits = {"habit1","habit2"};

        Human father = new Human("Sergei", "Ignishenko", 1960, 90, schedule);
        Human mother = new Human("Oksana", "Ignishenko", 1962, 130, schedule);
        Human child1 = new Human("Petro", "Ignishenko", 2001, 100, schedule);
        Human child2 = new Human("Maria", "Ignishenko", 1993, 110, schedule);
        Pet pet1 = new Pet(EnumSpecies.Dog, "Charlie", 7, 30, habits);

        Family family = new Family(father, mother);
        family.setPet(pet1);
        family.addChild(child1);
        family.addChild(child2);

        int expected = 4;
        assertEquals(expected, family.countFamily());
    }
}