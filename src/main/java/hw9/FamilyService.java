package hw9;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDAO familiesDB;

    public FamilyService(FamilyDAO families) {
        this.familiesDB = families;
    }

    List<Family> getAllFamilies(){
        return familiesDB.getAllFamilies();
    }

    String displayAllFamilies(){
        StringBuilder sb = new StringBuilder();
        familiesDB.getAllFamilies().forEach(x -> sb.append("Family ").append(familiesDB.getAllFamilies().indexOf(x)+1).append("\n").append(x.toString()).append("\n"));
        System.out.println(sb);
        return sb.toString();
    }

    List<Family> getFamiliesBiggerThan(int count){
        return familiesDB.getAllFamilies().stream()
                .filter(x -> x.countFamily()>count)
                .collect(Collectors.toList());
    }

    List<Family> getFamiliesLessThan(int count){
        return familiesDB.getAllFamilies().stream()
                .filter(x -> x.countFamily()<count)
                .collect(Collectors.toList());
    }

    int countFamiliesWithMemberNumber(int count){
        return Math.toIntExact(familiesDB.getAllFamilies().stream()
                .filter(x -> x.countFamily() == count)
                .count());
    }

    void createNewFamily(Human father, Human mother){
        Family family = new Family(father, mother);
        familiesDB.saveFamily(family);
    }

    void deleteFamilyByIndex(int id){
        familiesDB.deleteFamily(id);
    }

    void bornChild(Family family, String maleName, String femaleName){
        if(Math.random() > 0.5) family.addChild(new Man(maleName, family.getFather().getSurname(), 100, LocalDate.now()));
        else family.addChild(new Woman(femaleName, family.getFather().getSurname(), 100, LocalDate.now()));
    }

    void adoptChild(Family family, Human child){
        family.addChild(child);
    }

    void deleteAllChildrenOlderThan(int age){
        for (int i=0; i<familiesDB.getAllFamilies().size(); i++){
            for (int j = familiesDB.getFamilyByIndex(i+1).countFamily()-2; j>0; j--){
                if (familiesDB.getFamilyByIndex(i+1).getChildren().get(j-1).getBirthDate().isBefore((LocalDate.now().minusYears(age)))) {familiesDB.getFamilyByIndex(i+1).deleteChild(j);}
            }
        }
    }

    int count(){
        return familiesDB.getAllFamilies().size();
    }

    Family getFamilyById(int id){
        return familiesDB.getAllFamilies().get(id-1);
    }

    HashSet<Pet> getPets(int id){
        return familiesDB.getAllFamilies().get(id-1).getPets();
    }

    void addPet(int id, Pet pet){
        familiesDB.getAllFamilies().get(id-1).addPet(pet);
    }
}
