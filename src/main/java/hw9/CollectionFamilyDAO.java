package hw9;

import java.util.List;

public class CollectionFamilyDAO implements FamilyDAO {
    private List<Family> families;

    public CollectionFamilyDAO(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int id) {
        if (families.size()>=id) return families.get(id-1);
        else return null;
    }

    @Override
    public boolean deleteFamily(int id) {
        if (families.size()>=id){
            families.remove(id-1);
            return true;
        }
        else return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (families.contains(family)){
            families.remove(family);
            return true;
        }
        else return false;
    }

    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)){
            families.set(families.indexOf(family), family);
        }
        else families.add(family);
    }
}
