package hw9;

import java.util.HashSet;
import java.util.List;

public class FamilyController {
    FamilyService fc;

    public FamilyController(FamilyService fc) {
        this.fc = fc;
    }

    List<Family> getAllFamilies(){
        return fc.getAllFamilies();
    }

    void displayAllFamilies(){
        fc.displayAllFamilies();
    }

    List<Family> getFamiliesBiggerThan(int count){
        return fc.getFamiliesBiggerThan(count);
    }

    List<Family> getFamiliesLessThan(int count){
        return fc.getFamiliesLessThan(count);
    }

    int countFamiliesWithMemberNumber(int count){
        return fc.countFamiliesWithMemberNumber(count);
    }

    void createNewFamily(Human father, Human mother){
        fc.createNewFamily(father, mother);
    }

    void deleteFamilyByIndex(int id){
        fc.deleteFamilyByIndex(id);
    }

    void bornChild(Family family, String maleName, String femaleName){
        fc.bornChild(family, maleName, femaleName);
    }

    void adoptChild(Family family, Human child){
        fc.adoptChild(family, child);
    }

    void deleteAllChildrenOlderThan(int age){
        fc.deleteAllChildrenOlderThan(age);
    }

    int count(){
        return fc.count();
    }

    Family getFamilyById(int id){
        return fc.getFamilyById(id);
    }

    HashSet<Pet> getPets(int id){
        return fc.getPets(id);
    }

    void addPet(int id, Pet pet){
        fc.addPet(id, pet);
    }
}
