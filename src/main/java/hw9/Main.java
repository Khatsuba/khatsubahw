package hw9;

import java.time.LocalDate;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule1 = new HashMap();
        schedule1.put(DayOfWeek.Monday, "baseball");
        schedule1.put(DayOfWeek.Tuesday, "picnic");
        schedule1.put(DayOfWeek.Wednesday, "workout");
        schedule1.put(DayOfWeek.Thursday, "visit grandma");
        schedule1.put(DayOfWeek.Friday, "shopping");
        schedule1.put(DayOfWeek.Saturday, "yoga");
        schedule1.put(DayOfWeek.Sunday, "baseball");

        Set<String> habits1 = new HashSet<>();
        habits1.add("sleeps under bed");
        habits1.add("plays with kids");

        Set<String> habits2 = new HashSet<>();
        habits2.add("steals fish");

        //family 1
        Man father1 = new Man("Sergei", "Ignishenko", LocalDate.of(1962, 1, 1), 90, schedule1);
        Woman mother1 = new Woman("Oksana", "Ignishenko", LocalDate.of(1963, 1, 1), 130, schedule1);
        Man child11 = new Man("Petro", "Ignishenko", 90, "01/01/2005");
        Woman child12 = new Woman("Maria", "Ignishenko", 110, "01/01/1993");
        Dog pet1 = new Dog("Charlie", 7, 30, habits1);

        Family family1 = new Family(father1, mother1);
        family1.addPet(pet1);
        family1.addChild(child11);
        family1.addChild(child12);

        //family 2
        Man father2 = new Man("Ihor", "Shevchenko", LocalDate.of(1975, 1, 1), 110, schedule1);
        Woman mother2 = new Woman("Yulia", "Shevchenko", LocalDate.of(1973, 1, 1), 100, schedule1);
        Man child21 = new Man("Vlad", "Shevchenko", LocalDate.of(2001,1,1), 90, schedule1);
        Woman child22 = new Woman("Sophia", "Shevchenko", LocalDate.of(2004,1,1), 130, schedule1);
        Man child23 = new Man("Ivan", "Shevchenko", LocalDate.of(2005,1,1), 95, schedule1);
        Man child24 = new Man("Oleg", "Shevchenko", 100, "01/01/1990");
        DomesticCat pet2 = new DomesticCat("Luna", 2, 80, habits2);
        DomesticCat pet3 = new DomesticCat("Sima", 4, 20, habits2);

        Family family2 = new Family(father2, mother2);
        family2.addPet(pet2);
        family2.addPet(pet3);
        family2.addChild(child21);
        family2.addChild(child22);
        family2.addChild(child23);
        family2.addChild(child24);

        //family 3 - заготовка
        //Man father3 = new Man("Stepan", "Nikolenko", LocalDate.of(1970,1,1), 110, schedule1);
        //Woman mother3 = new Woman("Svetlana", "Nikolenko", LocalDate.of(1971,1,1), 100, schedule1);
        //Man child31 = new Man("Mihailo", "Kolechi", LocalDate.of(2000,1,1), 97, schedule1);
        //Dog pet4 = new Dog("Bobr",6,60,habits1);

        //----------------------------------------------------------------
        //                      FamilyController
        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService service = new FamilyService(dao);
        FamilyController fc = new FamilyController(service);

        //маніпуляції з сім'ями
        System.out.println("\nдві сім'ї задані спочатку:\n");
        fc.displayAllFamilies();

        System.out.println("\nвидалено всіх дітей старше 22:\n");
        fc.deleteAllChildrenOlderThan(22);
        fc.displayAllFamilies();

        father1.describeAge();
    }
}
