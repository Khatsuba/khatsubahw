package hw11;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Family {
    //characteristics
    private Human mother;    //+++
    private Human father;    //+++
    private List<Human> children;
    private HashSet<Pet> pets;

    //-----------------------------------------------------
    //consructor
    Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        this.father.setFamily(Family.this);
        this.mother.setFamily(Family.this);
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }

    //------------------------------------------------------
    //methods
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("father " + father.toString() + "\n");
        sb.append("mother " + mother.toString() + "\n");
        sb.append("children:\n");
        children.forEach(i -> sb.append(i.toString() + "\n"));
        pets.forEach(pet -> sb.append("pet " + pet.toString() +"\n"));
        return sb.toString();
    }

    public String prettyFormat(){
        StringBuilder sb = new StringBuilder();
        sb.append("     father: " + father.prettyFormat() + "\n");
        sb.append("     mother: " + mother.prettyFormat() + "\n");
        sb.append("     children:\n");
        children.forEach(i -> sb.append("           " + i.prettyFormat() + "\n"));
        sb.append("     pets: [");
        pets.forEach(i -> sb.append(i.prettyFormat() + ", "));
        sb.append("]\n");
        return sb.toString();
    }

    public void addChild(Human child){
        children.add(child);
        child.setFamily(this);
    }

    public void deleteChild(int id){
        if (children.size()>=id) children.remove(id-1);
        else{ System.out.println("child not found"); }
    }

    public void deleteChild(Human child){
        if (children.contains(child)) children.remove(child);
        else{ System.out.println("child not found"); }
    }

    public void addPet(Pet pet){
        pets.add(pet);
    }

    public void deletePet(int id){
        if (pets.size()>=id) pets.remove(id);
        else{ System.out.println("pet not found"); }
    }

    public void deletePet(Pet pet){
        if (pets.contains(pet)) pets.remove(pet);
        else{ System.out.println("pet not found"); }
    }

    public int countFamily(){
        return 2+children.size();
    }

    //-------------------------------------------------------
    //get set
    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public void setPets(HashSet<Pet> pets) {
        this.pets = pets;
    }

    //equals
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family family = (Family)obj;
        return father.equals(family.father) && mother.equals(family.mother);
    }

    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}
