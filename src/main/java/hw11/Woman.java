package hw11;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

public class Woman extends Human {
    Woman(String name, String surname, int iq, String age) {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        LocalDate birthDate;
        try {
            birthDate = format.parse(age).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        super.setName(name);
        super.setSurname(surname);
        super.setIq(iq);
        super.setBirthDate(birthDate);
        super.setSchedule(new HashMap<>());
    }
    Woman(String name, String surname, int iq, LocalDate birthDate) {
        super(name,surname,iq,birthDate);
    }
    Woman(String name, String surname, LocalDate birthDate, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }
    Woman() {
        super();
    }
    void greetPet(Pet pet){
        if (getFamily().getPets().contains(pet)){
            System.out.printf("Привіт, %s (я жінка)\n", pet.getNickname());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());
    }
    void makeup() {
        System.out.println("Чарівниця готується зачарувати світ: makeup в процесі.");
    }
}
