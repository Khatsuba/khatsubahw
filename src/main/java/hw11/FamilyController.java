package hw11;

import java.time.LocalDate;
import java.util.*;

public class FamilyController {
    FamilyService fs;

    public FamilyController(FamilyService fs) {
        this.fs = fs;
    }

    List<Family> getAllFamilies(){
        return fs.getAllFamilies();
    }

    void displayAllFamilies(){
        fs.displayAllFamilies();
    }

    String displayFamilyById(int id){
        return fs.displayFamilyById(id);
    }

    List<Family> getFamiliesBiggerThan(int count){
        return fs.getFamiliesBiggerThan(count);
    }

    List<Family> getFamiliesLessThan(int count){
        return fs.getFamiliesLessThan(count);
    }

    int countFamiliesWithMemberNumber(int count){
        return fs.countFamiliesWithMemberNumber(count);
    }

    void createNewFamily(Human father, Human mother){
        fs.createNewFamily(father, mother);
    }

    void deleteFamilyByIndex(int id){
        fs.deleteFamilyByIndex(id);
    }

    void bornChild(Family family, String maleName, String femaleName){
        try {
            fs.bornChild(family, maleName, femaleName);
        }
        catch (FamilyOverflowException ex) {
            System.out.println(ex.getMessage());
        }
    }

    void adoptChild(Family family, Human child){
        try {
            fs.adoptChild(family, child);
        }
        catch (FamilyOverflowException ex) {
            System.out.println(ex.getMessage());
        }

    }

    void deleteAllChildrenOlderThan(int age){
        fs.deleteAllChildrenOlderThan(age);
    }

    int count(){
        return fs.count();
    }

    Family getFamilyById(int id){
        return fs.getFamilyById(id);
    }

    HashSet<Pet> getPets(int id){
        return fs.getPets(id);
    }

    void addPet(int id, Pet pet){
        fs.addPet(id, pet);
    }

    Family testFamily1(){
        return fs.testFamily1();
    }
    Family testFamily2(){
        return fs.testFamily2();
    }
}
