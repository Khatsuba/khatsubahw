package hw11;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Human {
    //characteristics
    private String name;                      //+++
    private String surname;                   //+++
    private LocalDate birthDate;                         //+++
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;
    //-------------------------------------------------------------------------
    //constructors
    Human(String name, String surname, int iq, String age) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.schedule = new HashMap<>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.birthDate = format.parse(age).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    Human(String name, String surname, int iq, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = new HashMap<>();
    }
    Human(String name, String surname, LocalDate birthDate, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }
    Human() {

    }
    //-------------------------------------------------------------------------
    //methods
    void describeAge(){
        System.out.printf("мені %s років, %s місяців та %s днів",
                Period.between(birthDate, LocalDate.now()).getYears(),
                Period.between(birthDate, LocalDate.now()).getMonths(),
                Period.between(birthDate, LocalDate.now()).getDays() );
    }
    void greetPet(Pet pet){
        if (family.getPets().contains(pet)){
            System.out.printf("Привіт, %s \n", pet.getNickname());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());

    }
    void describePet(Pet pet){
        if (family.getPets().contains(pet)){
            if (pet.getTricklevel()>50){
                System.out.printf("У мене є %s, %s, їй %s років, він дуже хитрий\n", pet.getSpecies(), pet.getNickname(), pet.getAge());
            }
            else System.out.printf("У мене є %s, %s, їй %s років, він майже не хитрий\n", pet.getSpecies(), pet.getNickname(), pet.getAge());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());

    }

    String prettyFormat(){
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
        sb.append(String.format("{name='%s', surname='%s', birthDate=%s, iq=%s, schedule={ Monday=%s, Tuesday=%s, Wednesday=%s, Thursday=%s, Friday=%s, Saturday=%s, Sunday=%s}",
                name,
                surname,
                f1.format(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant())),
                iq,
                getSchedule().get(DayOfWeek.Monday),
                getSchedule().get(DayOfWeek.Tuesday),
                getSchedule().get(DayOfWeek.Wednesday),
                getSchedule().get(DayOfWeek.Thursday),
                getSchedule().get(DayOfWeek.Friday),
                getSchedule().get(DayOfWeek.Saturday),
                getSchedule().get(DayOfWeek.Sunday)
        ));
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
        sb.append(String.format("Human{name='%s', surname='%s', year=%s, iq=%s, schedule=[ [ Monday,%s ] [ Tuesday,%s ] [ Wednesday,%s ] [ Thursday,%s ] [ Friday,%s ] [ Saturday,%s ] [ Sunday,%s ]",
                name,
                surname,
                f1.format(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant())),
                iq,
                getSchedule().get(DayOfWeek.Monday),
                getSchedule().get(DayOfWeek.Tuesday),
                getSchedule().get(DayOfWeek.Wednesday),
                getSchedule().get(DayOfWeek.Thursday),
                getSchedule().get(DayOfWeek.Friday),
                getSchedule().get(DayOfWeek.Saturday),
                getSchedule().get(DayOfWeek.Sunday)
        ));
        sb.append(" ]}");
        return sb.toString();
    }
    //--------------------------------------------------------------------------------------------------
    //get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    //equals
    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human human = (Human)obj;
        return name == human.name && surname == human.surname && birthDate == human.birthDate;
    }

    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}
