package hw11;

public enum EnumSpecies {
    RobotCat,
    DomesticCat,
    Dog,
    Fish,
    UNKNOWN
}