package hw11;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDAO familiesDB;

    public FamilyService(FamilyDAO families) {
        this.familiesDB = families;
    }

    List<Family> getAllFamilies(){
        return familiesDB.getAllFamilies();
    }

    String displayAllFamilies(){
        StringBuilder sb = new StringBuilder();
        familiesDB.getAllFamilies()
                .forEach(x -> sb.append("Family ").append(familiesDB.getAllFamilies().indexOf(x)+1).append("\n").append(x.prettyFormat()).append("\n"));
        System.out.println(sb);
        return sb.toString();
    }

    String displayFamilyById(int id){
        StringBuilder sb = new StringBuilder();
        sb.append("Family ").append(id).append("\n").append(familiesDB.getFamilyByIndex(id).prettyFormat()).append("\n");
        System.out.println(sb);
        return sb.toString();
    }

    List<Family> getFamiliesBiggerThan(int count){
        return familiesDB.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily()>count)
                .collect(Collectors.toList());
    }

    List<Family> getFamiliesLessThan(int count){
        return familiesDB.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily()<count)
                .collect(Collectors.toList());
    }

    int countFamiliesWithMemberNumber(int count){
        return Math.toIntExact(familiesDB.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() == count)
                .count());
    }

    void createNewFamily(Human father, Human mother){
        Family family = new Family(father, mother);
        familiesDB.saveFamily(family);
    }

    void deleteFamilyByIndex(int id){
        familiesDB.deleteFamily(id);
    }

    void bornChild(Family family, String maleName, String femaleName){
        if (family.countFamily() < 6)
        {
            if(Math.random() > 0.5) family.addChild(new Man(maleName, family.getFather().getSurname(), 100, LocalDate.now()));
            else family.addChild(new Woman(femaleName, family.getFather().getSurname(), 100, LocalDate.now()));
        }
        else throw new FamilyOverflowException("В сім'ї максимальна кількість членів (6)");
    }

    void adoptChild(Family family, Human child) throws FamilyOverflowException {
        if (family.countFamily() < 6)
        {
            family.addChild(child);
        }
        else throw new FamilyOverflowException("В сім'ї максимальна кількість членів (6)");
    }

    void deleteAllChildrenOlderThan(int age){

        familiesDB.getAllFamilies().forEach(x-> {
            x.setChildren((ArrayList<Human>) x.getChildren()
                    .stream()
                    .filter((y) -> y.getBirthDate().isAfter((LocalDate.now().minusYears(age))))
                    .collect(Collectors.toList())
            );
        }
        );
    }

    int count(){
        return familiesDB.getAllFamilies().size();
    }

    Family getFamilyById(int id){
        return familiesDB.getAllFamilies().get(id-1);
    }

    HashSet<Pet> getPets(int id){
        return familiesDB.getAllFamilies().get(id-1).getPets();
    }

    void addPet(int id, Pet pet){
        familiesDB.getAllFamilies().get(id-1).addPet(pet);
    }

    Family testFamily1(){
        Map<DayOfWeek, String> schedule1 = new HashMap<>();
        schedule1.put(DayOfWeek.Monday, "baseball");
        schedule1.put(DayOfWeek.Tuesday, "picnic");
        schedule1.put(DayOfWeek.Wednesday, "workout");
        schedule1.put(DayOfWeek.Thursday, "visit grandma");
        schedule1.put(DayOfWeek.Friday, "shopping");
        schedule1.put(DayOfWeek.Saturday, "yoga");
        schedule1.put(DayOfWeek.Sunday, "baseball");

        Set<String> habits1 = new HashSet<>();
        habits1.add("sleeps under bed");
        habits1.add("plays with kids");

        //family 1
        Man father1 = new Man("Sergei", "Ignishenko", LocalDate.of(1962, 1, 1), 90, schedule1);
        Woman mother1 = new Woman("Oksana", "Ignishenko", LocalDate.of(1963, 1, 1), 130, schedule1);
        Man child11 = new Man("Petro", "Ignishenko", 90, "01/01/2005");
        Woman child12 = new Woman("Maria", "Ignishenko", 110, "01/01/1993");
        Dog pet1 = new Dog("Charlie", 7, 30, habits1);

        Family family1 = new Family(father1, mother1);
        family1.addPet(pet1);
        family1.addChild(child11);
        family1.addChild(child12);

        return family1;
    }
    Family testFamily2(){
        Map<DayOfWeek, String> schedule1 = new HashMap<>();
        schedule1.put(DayOfWeek.Monday, "baseball");
        schedule1.put(DayOfWeek.Tuesday, "picnic");
        schedule1.put(DayOfWeek.Wednesday, "workout");
        schedule1.put(DayOfWeek.Thursday, "visit grandma");
        schedule1.put(DayOfWeek.Friday, "shopping");
        schedule1.put(DayOfWeek.Saturday, "yoga");
        schedule1.put(DayOfWeek.Sunday, "baseball");

        Set<String> habits2 = new HashSet<>();
        habits2.add("steals fish");

        Man father2 = new Man("Ihor", "Shevchenko", LocalDate.of(1975, 1, 1), 110, schedule1);
        Woman mother2 = new Woman("Yulia", "Shevchenko", LocalDate.of(1973, 1, 1), 100, schedule1);
        Man child21 = new Man("Vlad", "Shevchenko", LocalDate.of(2001, 1, 1), 90, schedule1);
        Woman child22 = new Woman("Sophia", "Shevchenko", LocalDate.of(2004, 1, 1), 130, schedule1);
        Man child23 = new Man("Ivan", "Shevchenko", LocalDate.of(2005, 1, 1), 95, schedule1);
        Man child24 = new Man("Oleg", "Shevchenko", 100, "01/01/1990");
        DomesticCat pet2 = new DomesticCat("Luna", 2, 80, habits2);
        DomesticCat pet3 = new DomesticCat("Sima", 4, 20, habits2);

        Family family2 = new Family(father2, mother2);
        family2.addPet(pet2);
        family2.addPet(pet3);
        family2.addChild(child21);
        family2.addChild(child22);
        family2.addChild(child23);
        family2.addChild(child24);

        return family2;
    }
}
