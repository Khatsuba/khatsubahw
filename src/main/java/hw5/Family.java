package hw5;

public class Family {
    //characteristics
    private Human mother;    //+++
    private Human father;    //+++
    private Human[] children;
    private Pet pet;

    //-----------------------------------------------------
    //consructor
    Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        this.father.setFamily(Family.this);
        this.mother.setFamily(Family.this);
        this.children = new Human[]{};
    }

    //------------------------------------------------------
    //methods
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("father " + father.toString() + "\n");
        sb.append("mother " + mother.toString() + "\n");
        sb.append("children:\n");
        for (int i=0; i<children.length; i++) {
            sb.append(children[i].toString() + "\n");
        }
        sb.append("pet " + pet.toString() +"\n");
        return sb.toString();
    }

    public void addChild(Human child){
        Human[] buffer = new Human[children.length+1];
        for (int i=0; i<buffer.length-1; i++) {
            buffer[i] = children[i];
        }
        buffer[buffer.length-1] = child;
        children = buffer;
        child.setFamily(Family.this);
    }

    public void deleteChild(int id){
        if (children.length>=id){
            Human[] buffer = new Human[children.length-1];
            for (int i=0; i<buffer.length; i++) {
                if (i<id-1){ buffer[i] = children[i]; }
                else { buffer[i] = children[i+1]; }
            }
            children = buffer;
        }
        else{ System.out.println("child not found"); }
    }

    public void deleteChild(Human child){
        for (int i=0; i<children.length; i++) {
            if (children[i].equals(child)) {
                int id = i+1;
                Human[] buffer = new Human[children.length-1];
                for (int j=0; j<buffer.length; j++) {
                    if (j<id-1){ buffer[j] = children[j]; }
                    else { buffer[j] = children[j+1]; }
                }
                children = buffer;
                break;
            }
        }
    }

    public int countFamily(){
        return 2+children.length;
    }

    //-------------------------------------------------------
    //get set
    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
    //equals
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family family = (Family)obj;
        return father.equals(family.father) && mother.equals(family.mother);
    }

    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}
