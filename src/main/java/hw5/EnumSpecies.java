package hw5;

public enum EnumSpecies {
    Parrot,
    Cat,
    Dog,
    Fish,
    Snake,
    Frog,
    Turtle,
    Rabbit,
    Pig
}