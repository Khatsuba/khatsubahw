package hw5;

public class Human {
    //characteristics
    private String name;                      //+++
    private String surname;                   //+++
    private int year;                         //+++
    private int iq;
    private String[][] schedule;
    private Family family;
    //-------------------------------------------------------------------------
    //constructors
    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    Human() {

    }
    //-------------------------------------------------------------------------
    //methods
    void greetPet(){
        System.out.printf("Привіт, %s \n", family.getPet().getNickname());
    }
    void describePet(){
        if (family.getPet().getTricklevel()>50){
            System.out.printf("У мене є %s, %s, їй %s років, він дуже хитрий\n", family.getPet().getSpecies(), family.getPet().getNickname(), family.getPet().getAge());
        }
        else {
            System.out.printf("У мене є %s, %s, їй %s років, він майже не хитрий\n", family.getPet().getSpecies(), family.getPet().getNickname(), family.getPet().getAge());
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Human{name='%s', surname='%s', year=%s, iq=%s, schedule=[",
                name,
                surname,
                year,
                iq));
        for (int i=0; i<schedule.length; i++) {
            sb.append(" [ " + schedule[i][0] + "," + schedule[i][1] + " ]");
        }
        sb.append(" ]}");
        return sb.toString();
    }
    //--------------------------------------------------------------------------------------------------
    //get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    //equals
    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human human = (Human)obj;
        return name == human.name && surname == human.surname && year == human.year;
    }

    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}
