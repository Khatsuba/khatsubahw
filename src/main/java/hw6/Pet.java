package hw6;

import java.util.Arrays;

public abstract class Pet {
    //characteristics
    EnumSpecies species = EnumSpecies.UNKNOWN;                          //+++
    String nickname;                         //+++
    int age;
    int tricklevel;
    String[] habits;
    //-------------------------------------------------------------------------
    //constructors
    Pet(EnumSpecies species, String nickname, int age, int tricklevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.tricklevel = tricklevel;
        this.habits = habits;
    }

    Pet(EnumSpecies species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(){

    }
    //-------------------------------------------------------------------------
    //methods
    void eat(){
        System.out.println("Я ї'м!");
    }
    abstract void respond();
    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%s, trickLevel=%s, habits=%s}",
                species,
                nickname,
                age,
                tricklevel,
                Arrays.toString(habits));
    }
    //--------------------------------------------------------------------------
    //get set
    public String getSpecies() {
        return species.toString();
    }
    public void setSpecies(EnumSpecies species) {
        this.species = species;
    }
    public String getNickname() {
        return  nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getTricklevel() {
        return tricklevel;
    }
    public void setTricklevel(int tricklevel) {
        this.tricklevel = tricklevel;
    }
    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    //equals
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Pet))
            return false;
        Pet pet = (Pet)obj;
        return species == pet.species && nickname == pet.nickname;
    }
    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}

