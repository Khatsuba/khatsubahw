package hw6;

public enum EnumSpecies {
    RobotCat,
    DomesticCat,
    Dog,
    Fish,
    UNKNOWN
}