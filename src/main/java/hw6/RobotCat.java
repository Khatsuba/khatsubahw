package hw6;

import java.util.Arrays;

public class RobotCat extends Pet{
    //-------------------------------------------------------------------------
    //constructors
    RobotCat(EnumSpecies species, String nickname) {
        super(EnumSpecies.RobotCat,nickname);
    }
    RobotCat(String nickname, int age, int tricklevel, String[] habits) {
        super(EnumSpecies.RobotCat,nickname,age,tricklevel,habits);
    }
    RobotCat() {
        super();
    }
    //-------------------------------------------------------------------------
    //methods
    void respond(){
        System.out.printf("Привіт, хазяїн. Я - робокіт %s. Очікую команди!\n", this.nickname);
    }
}

