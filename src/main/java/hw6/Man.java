package hw6;

public class Man extends Human{
    Man(String name, String surname, int year) {
        super(name,surname,year);
    }
    Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }
    Man() {
        super();
    }

    void greetPet(){
        System.out.printf("Привіт, %s (я чоловік)\n", getFamily().getPet().getNickname());
    }

    void fixCar() {
        System.out.println("Майстер автомеханік у дії: fixCar в процесі.");
    }
}
