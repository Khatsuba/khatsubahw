package hw6;

public class Woman extends Human{
    Woman(String name, String surname, int year) {
        super(name,surname,year);
    }
    Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }
    Woman() {
        super();
    }
    void greetPet(){
        System.out.printf("Привіт, %s (я жінка)\n", getFamily().getPet().getNickname());
    }
    void makeup() {
        System.out.println("Чарівниця готується зачарувати світ: makeup в процесі.");
    }
}
