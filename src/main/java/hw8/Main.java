package hw8;

import java.sql.Array;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule1 = new HashMap();
        schedule1.put(DayOfWeek.Monday, "baseball");
        schedule1.put(DayOfWeek.Tuesday, "picnic");
        schedule1.put(DayOfWeek.Wednesday, "workout");
        schedule1.put(DayOfWeek.Thursday, "visit grandma");
        schedule1.put(DayOfWeek.Friday, "shopping");
        schedule1.put(DayOfWeek.Saturday, "yoga");
        schedule1.put(DayOfWeek.Sunday, "baseball");

        Set<String> habits1 = new HashSet<>();
        habits1.add("sleeps under bed");
        habits1.add("plays with kids");

        Set<String> habits2 = new HashSet<>();
        habits2.add("steals fish");

        //family 1
        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule1);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule1);
        Man child11 = new Man("Petro", "Ignishenko", 2001, 100, schedule1);
        Woman child12 = new Woman("Maria", "Ignishenko", 1993, 110, schedule1);
        Dog pet1 = new Dog("Charlie", 7, 30, habits1);

        Family family1 = new Family(father1, mother1);
        family1.addPet(pet1);
        family1.addChild(child11);
        family1.addChild(child12);

        //family 2
        Man father2 = new Man("Ihor", "Shevchenko", 1975, 110, schedule1);
        Woman mother2 = new Woman("Yulia", "Shevchenko", 1973, 100, schedule1);
        Man child21 = new Man("Vlad", "Shevchenko", 2001, 90, schedule1);
        Woman child22 = new Woman("Sophia", "Shevchenko", 2004, 130, schedule1);
        Man child23 = new Man("Ivan", "Shevchenko", 2005, 95, schedule1);
        Man child24 = new Man("Oleg", "Shevchenko", 1990, 100, schedule1);
        DomesticCat pet2 = new DomesticCat("Luna", 2, 80, habits2);
        DomesticCat pet3 = new DomesticCat("Sima", 4, 20, habits2);

        Family family2 = new Family(father2, mother2);
        family2.addPet(pet2);
        family2.addPet(pet3);
        family2.addChild(child21);
        family2.addChild(child22);
        family2.addChild(child23);
        family2.addChild(child24);

        //family 3 - заготовка
        Man father3 = new Man("Stepan", "Nikolenko", 1970, 110, schedule1);
        Woman mother3 = new Woman("Svetlana", "Nikolenko", 1971, 100, schedule1);
        Man child31 = new Man("Mihailo", "Kolechi", 2000, 97, schedule1);
        Dog pet4 = new Dog("Bobr", 6, 60, habits1);

        //----------------------------------------------------------------
        //                      FamilyController
        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        dao.saveFamily(family1);
        dao.saveFamily(family2);
        FamilyService service = new FamilyService(dao);
        FamilyController fc = new FamilyController(service);

        //маніпуляції з сім'ями
        System.out.println("\nдві сім'ї задані спочатку:\n");
        fc.displayAllFamilies();

        System.out.println(
                "проведено послідовність дій:" +
                "\nстворено нову сім'ю" +
                "\nвидалено сім'ю з індексом 2 (family2)" +
                "\nнароджено дитину в сім'ї з індексом 2" +
                "\nусиновлено дитину child 31 у сім'ю з індексом 2" +
                "\nдодано тварину pet4 ло сім'ї з індексом 2\n");

        fc.createNewFamily(father1, mother2);
        fc.deleteFamilyByIndex(2);
        fc.bornChild(dao.getFamilyByIndex(2), "Danilo", "Anna");
        fc.adoptChild(dao.getFamilyByIndex(2), child31);
        fc.addPet(2, pet4);
        fc.displayAllFamilies();

        System.out.println("\nвидалено всіх дітей старше 22:\n");
        fc.deleteAllChildrenOlderThan(22);
        fc.displayAllFamilies();

        //інші операції
        System.out.println("кількість сімей з 2/3/4 членів: "
                + fc.countFamiliesWithMemberNumber(2) + " "
                + fc.countFamiliesWithMemberNumber(3) + " "
                + fc.countFamiliesWithMemberNumber(4) + " ");

        System.out.println("кількість сімей загалом: " + fc.count());

        //треба змінити склад сімей щоб вони не були з однаковою кількістю членів
        fc.bornChild(dao.getFamilyByIndex(2), "boy", "girl");
        fc.bornChild(dao.getFamilyByIndex(2), "boy", "girl");

        System.out.println("\nlist сімей де менше 4 членів:\n" + fc.getFamiliesLessThan(4));
        System.out.println("\nlist сімей де більше 4 членів:\n" + fc.getFamiliesBiggerThan(4));
        System.out.println("\nlist усіх сімей:\n" + fc.getAllFamilies());
        System.out.println("\nсім'я за індексом 1:\n" + fc.getFamilyById(1));
        System.out.println("\nтварини сім'ї за індексом 2:\n" + fc.getPets(2));
    }
}
