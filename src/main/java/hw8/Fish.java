package hw8;

import java.util.Set;

public class Fish extends Pet {
    //-------------------------------------------------------------------------
    //constructors
    Fish(EnumSpecies species, String nickname) {
        super(EnumSpecies.Fish,nickname);
    }
    Fish(String nickname, int age, int tricklevel, Set<String> habits) {
        super(EnumSpecies.Fish,nickname,age,tricklevel,habits);
    }
    Fish() {
        super();
    }
    //-------------------------------------------------------------------------
    //methods
    void respond(){
        System.out.printf("Привіт, хазяїн. Я - риба %s. Буль буль!\n", this.nickname);
    }
}

