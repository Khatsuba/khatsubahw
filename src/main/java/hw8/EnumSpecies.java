package hw8;

public enum EnumSpecies {
    RobotCat,
    DomesticCat,
    Dog,
    Fish,
    UNKNOWN
}