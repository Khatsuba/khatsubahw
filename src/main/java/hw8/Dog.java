package hw8;

import java.util.Set;

public class Dog extends Pet implements InterfaceFoul {
    //-------------------------------------------------------------------------
    //constructors
    Dog(EnumSpecies species, String nickname) {
        super(EnumSpecies.Dog,nickname);
    }
    Dog(String nickname, int age, int tricklevel, Set<String> habits) {
        super(EnumSpecies.Dog,nickname,age,tricklevel,habits);
    }
    Dog() {
        super();
    }
    //-------------------------------------------------------------------------
    //methods
    void respond(){
        System.out.printf("Привіт, хазяїн. Я - собака %s. Я скучив!\n", this.nickname);
    }
    @Override
    public void foul() {
        System.out.println("Потрібно добре замести...");
    }
}

