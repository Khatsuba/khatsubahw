package hw12;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

public class Human {

    //characteristics
    private String name;                      //+++
    private String surname;                   //+++
    private LocalDate birthDate;                         //+++
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;
    //-------------------------------------------------------------------------
    //constructors
    Human(String name, String surname, int iq, String age) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.schedule = new HashMap<>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.birthDate = format.parse(age).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    Human(String name, String surname, int iq, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = new HashMap<>();
    }
    Human(String name, String surname, LocalDate birthDate, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }
    Human() {

    }
    //-------------------------------------------------------------------------
    //methods

    public String serialize(){
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");

        return this.name +
                ";" +
                this.surname +
                ";" +
                f1.format(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant())) +
                ";" +
                this.iq +
                ";" +
                this.schedule.get(DayOfWeek.Monday) +
                ";" +
                this.schedule.get(DayOfWeek.Tuesday) +
                ";" +
                this.schedule.get(DayOfWeek.Wednesday) +
                ";" +
                this.schedule.get(DayOfWeek.Thursday) +
                ";" +
                this.schedule.get(DayOfWeek.Friday) +
                ";" +
                this.schedule.get(DayOfWeek.Saturday) +
                ";" +
                this.schedule.get(DayOfWeek.Sunday);
    }

    public static Human deserialize(String in){
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
        ArrayList<String> list = new ArrayList<>(List.of(in.split(";")));

        HashMap<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.Monday, list.get(4));
        schedule.put(DayOfWeek.Tuesday, list.get(5));
        schedule.put(DayOfWeek.Wednesday, list.get(6));
        schedule.put(DayOfWeek.Thursday, list.get(7));
        schedule.put(DayOfWeek.Friday, list.get(8));
        schedule.put(DayOfWeek.Saturday, list.get(9));
        schedule.put(DayOfWeek.Sunday, list.get(10));

        try {
            return new Human(
                    list.get(0),
                    list.get(1),
                    f1.parse(list.get(2)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                    Integer.parseInt(list.get(3)),
                    schedule);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    void describeAge(){
        System.out.printf("мені %s років, %s місяців та %s днів",
                Period.between(birthDate, LocalDate.now()).getYears(),
                Period.between(birthDate, LocalDate.now()).getMonths(),
                Period.between(birthDate, LocalDate.now()).getDays() );
    }
    void greetPet(Pet pet){
        if (family.getPets().contains(pet)){
            System.out.printf("Привіт, %s \n", pet.getNickname());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());

    }
    void describePet(Pet pet){
        if (family.getPets().contains(pet)){
            if (pet.getTricklevel()>50){
                System.out.printf("У мене є %s, %s, їй %s років, він дуже хитрий\n", pet.getSpecies(), pet.getNickname(), pet.getAge());
            }
            else System.out.printf("У мене є %s, %s, їй %s років, він майже не хитрий\n", pet.getSpecies(), pet.getNickname(), pet.getAge());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());

    }

    String prettyFormat(){
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
        return String.format("{name='%s', surname='%s', birthDate=%s, iq=%s, schedule={ Monday=%s, Tuesday=%s, Wednesday=%s, Thursday=%s, Friday=%s, Saturday=%s, Sunday=%s}",
                name,
                surname,
                f1.format(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant())),
                iq,
                getSchedule().get(DayOfWeek.Monday),
                getSchedule().get(DayOfWeek.Tuesday),
                getSchedule().get(DayOfWeek.Wednesday),
                getSchedule().get(DayOfWeek.Thursday),
                getSchedule().get(DayOfWeek.Friday),
                getSchedule().get(DayOfWeek.Saturday),
                getSchedule().get(DayOfWeek.Sunday)
        );
    }

    @Override
    public String toString() {
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
        return String.format("Human{name='%s', surname='%s', year=%s, iq=%s, schedule=[ [ Monday,%s ] [ Tuesday,%s ] [ Wednesday,%s ] [ Thursday,%s ] [ Friday,%s ] [ Saturday,%s ] [ Sunday,%s ]",
                name,
                surname,
                f1.format(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant())),
                iq,
                getSchedule().get(DayOfWeek.Monday),
                getSchedule().get(DayOfWeek.Tuesday),
                getSchedule().get(DayOfWeek.Wednesday),
                getSchedule().get(DayOfWeek.Thursday),
                getSchedule().get(DayOfWeek.Friday),
                getSchedule().get(DayOfWeek.Saturday),
                getSchedule().get(DayOfWeek.Sunday)
        ) +
                " ]}";
    }
    //--------------------------------------------------------------------------------------------------
    //get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    //equals
    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human human = (Human)obj;
        return name == human.name && surname == human.surname && birthDate == human.birthDate;
    }

    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}
