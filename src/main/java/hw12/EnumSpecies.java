package hw12;

public enum EnumSpecies {
    RobotCat,
    DomesticCat,
    Dog,
    Fish,
    UNKNOWN
}