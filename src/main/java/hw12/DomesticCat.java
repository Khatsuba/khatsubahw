package hw12;

import java.util.Set;

public class DomesticCat extends Pet implements InterfaceFoul {
    //-------------------------------------------------------------------------
    //constructors
    DomesticCat(EnumSpecies species, String nickname) {
        super(EnumSpecies.DomesticCat,nickname);
    }
    DomesticCat(String nickname, int age, int tricklevel, Set<String> habits) {
        super(EnumSpecies.DomesticCat,nickname,age,tricklevel,habits);
    }
    DomesticCat() {
        super();
    }
    //-------------------------------------------------------------------------
    //methods
    void respond(){
        System.out.printf("Привіт, хазяїн. Я кіт - %s. Я скучив!\n", this.nickname);
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}

