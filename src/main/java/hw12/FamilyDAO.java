package hw12;

import java.util.List;

public interface FamilyDAO {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int id);
    boolean deleteFamily(int id);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);
    void loadData(List<Family> families);
}
