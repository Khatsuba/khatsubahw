package hw12;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDAO {
    private List<Family> families;

    public CollectionFamilyDAO(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int id) {
        if (families.size()>=id) return families.get(id-1);
        else return null;
    }

    @Override
    public boolean deleteFamily(int id) {
        if (families.size()>=id){
            families.remove(id-1);
            return true;
        }
        else return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (families.contains(family)){
            families.remove(family);
            return true;
        }
        else return false;
    }

    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)){
            families.set(families.indexOf(family), family);
        }
        else families.add(family);
    }

    public void loadData(List<Family> families){
        File f = new File("D:\\test\\test.txt");
        StringBuilder sb = new StringBuilder();
        for (Family family : families){

            sb.append(family.getFather().serialize());
            sb.append("/\r\n");
            sb.append(family.getMother().serialize());
            sb.append("/\r\n");
            for (Human c : family.getChildren()){
                if (c instanceof Man) sb.append("boy");
                else sb.append("girl");
                sb.append(";;;");
                sb.append(c.serialize());
                sb.append("\r\n");
            }
            sb.append("/\r\n");
            for (Pet p : family.getPets()){
                sb.append(p.serialize());
                sb.append("\r\n");
            }
            sb.append("//\r\n");
        }

        try {
            FileWriter fw = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(fw);

            f.delete();
            bw.write(sb.toString());
            bw.close();
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
}
