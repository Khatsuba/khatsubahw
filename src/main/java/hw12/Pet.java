package hw12;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

public abstract class Pet {
    //characteristics
    EnumSpecies species = EnumSpecies.UNKNOWN;                          //+++
    String nickname;                         //+++
    int age;
    int tricklevel;
    Set<String> habits;
    //-------------------------------------------------------------------------
    //constructors
    Pet(EnumSpecies species, String nickname, int age, int tricklevel, Set<String> habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.tricklevel = tricklevel;
        this.habits = habits;
    }

    Pet(EnumSpecies species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(){

    }
    //-------------------------------------------------------------------------
    //methods
    public String serialize(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.species).append(";").append(this.nickname).append(";").append(this.age).append(";").append(this.tricklevel);
        for (String x : habits) {
            sb.append(";").append(x);
        }
        return sb.toString();
    }

    public static Pet deserialize(String in){
        ArrayList<String> list = new ArrayList<>(List.of(in.split(";")));

        Set<String> habits = new HashSet<>();
        for (int i=4; i<list.size(); i++)habits.add(list.get(i));
        switch (list.get(0)){
            case "RobotCat":
                return new RobotCat(
                        list.get(1),
                        Integer.parseInt(list.get(2)),
                        Integer.parseInt(list.get(3)),
                        habits);
            case "DomesticCat":
                return new DomesticCat(
                        list.get(1),
                        Integer.parseInt(list.get(2)),
                        Integer.parseInt(list.get(3)),
                        habits);
            case "Dog":
                return new Dog(
                        list.get(1),
                        Integer.parseInt(list.get(2)),
                        Integer.parseInt(list.get(3)),
                        habits);
            case "Fish":
                return new Fish(
                        list.get(1),
                        Integer.parseInt(list.get(2)),
                        Integer.parseInt(list.get(3)),
                        habits);
            default:
                System.out.println("ALERT ALERT ALERT");
                System.out.println(list.get(0));
                return null;
        }
    }
    void eat(){
        System.out.println("Я ї'м!");
    }
    abstract void respond();

    String prettyFormat(){
        return String.format("{species=%s, nickname='%s', age=%s, trickLevel=%s, habits=%s}",
                species,
                nickname,
                age,
                tricklevel,
                habits.toString());
    }
    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%s, trickLevel=%s, habits=%s}",
                species,
                nickname,
                age,
                tricklevel,
                habits.toString());
    }


    //--------------------------------------------------------------------------
    //get set
    public String getSpecies() {
        return species.toString();
    }
    public void setSpecies(EnumSpecies species) {
        this.species = species;
    }
    public String getNickname() {
        return  nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getTricklevel() {
        return tricklevel;
    }
    public void setTricklevel(int tricklevel) {
        this.tricklevel = tricklevel;
    }
    public Set<String> getHabits() {
        return habits;
    }
    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    //equals
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Pet))
            return false;
        Pet pet = (Pet)obj;
        return species == pet.species && nickname == pet.nickname;
    }
    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}

