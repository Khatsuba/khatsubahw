package hw12;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //----------------------------------------------------------------
        //                      FamilyController
        FamilyDAO dao = new CollectionFamilyDAO(new ArrayList<>());
        FamilyService service = new FamilyService(dao);
        FamilyController fc = new FamilyController(service);

        //----------------------------------------------------------------
        //                      Console program
        Scanner sc = new Scanner(System.in);
        int intInput;
        String stringInput;
        String help =
                        "1. Заповнити тестовими даними (автоматом створити кілька сімей та зберегти їх у базі)\n" +
                        "2. Відобразити весь список сімей (відображає список усіх сімей з індексацією, що починається з 1)\n" +
                        "3. Відобразити список сімей, де кількість людей більша за задану\n" +
                        "4. Відобразити список сімей, де кількість людей менша за задану\n" +
                        "5. Підрахувати кількість сімей, де кількість членів дорівнює\n" +
                        "6. Створити нову родину\n" +
                        "7. Видалити сім'ю за індексом сім'ї у загальному списку\n" +
                        "8. Редагувати сім'ю за індексом сім'ї у загальному списку\n" +
                        "- 1. Народити дитину\n" +
                        "- 2. Усиновити дитину\n" +
                        "- 3. Повернутися до головного меню\n" +
                        "9. Видалити всіх дітей старше віку (у всіх сім'ях видаляються діти старше зазначеного віку - вважатимемо, що вони виросли)\n" +
                        "10. Завантажити сім'ї з файлу\n" +
                        "11. Вивантажити сім'ї у файл\n";

        //----------------------------------------------------------------
        while(true) {
            System.out.println(help);
            System.out.println("Введіть команду:");
            do {
                while (!sc.hasNextInt()) {
                    System.out.print("такої команди немає, спробуй ще раз:\n");
                    sc.next();
                }
                intInput = sc.nextInt();
                if (intInput > 11 || intInput < 1) System.out.println("такої команди немає, спробуй ще раз:");
            } while (intInput > 11 || intInput < 1);

            switch (intInput) {

                case 1:
                    dao.saveFamily(fc.testFamily1());
                    dao.saveFamily(fc.testFamily2());
                    System.out.println("створено дві тестові сім'ї\n");
                    break;

                //----------------------------------------------------------------
                case 2:
                    fc.displayAllFamilies();
                    break;

                //----------------------------------------------------------------
                case 3:
                {
                    System.out.println("введіть число:");
                    while (!sc.hasNextInt()) {
                        System.out.print("потрібно ввести число, спробуй ще раз:\n");
                        sc.next();
                    }
                    intInput = sc.nextInt();
                    int finalIntInput1 = intInput;
                    dao.getAllFamilies()
                            .stream()
                            .filter(x -> x.countFamily() > finalIntInput1)
                            .forEach(x -> System.out.println("Family " + (dao.getAllFamilies().indexOf(x) + 1) + "\n" + x.prettyFormat()));
                    break;
                }

                //----------------------------------------------------------------
                case 4:
                {
                    System.out.println("введіть число:");
                    while (!sc.hasNextInt()) {
                        System.out.print("потрібно ввести число, спробуй ще раз:\n");
                        sc.next();
                    }
                    intInput = sc.nextInt();
                    int finalIntInput2 = intInput;
                    dao.getAllFamilies()
                            .stream()
                            .filter(x -> x.countFamily() < finalIntInput2)
                            .forEach(x -> System.out.println("Family " + (dao.getAllFamilies().indexOf(x) + 1) + "\n" + x.prettyFormat()));
                    break;
                }

                //----------------------------------------------------------------
                case 5:
                {
                    System.out.println("введіть число:");
                    while (!sc.hasNextInt()) {
                        System.out.print("потрібно ввести число, спробуй ще раз:\n");
                        sc.next();
                    }
                    intInput = sc.nextInt();
                    System.out.println("Кількість сімей з " + intInput + " членів: " + fc.countFamiliesWithMemberNumber(intInput));
                    break;
                }

                //----------------------------------------------------------------
                case 6:
                {
                    //батько---------------------------------------------------------------
                    System.out.println("введіть ім'я батька:");
                    while (sc.hasNextInt()) {
                        System.out.print("не можна назвати людину номером, спробуй ще раз:\n");
                        sc.next();
                    }
                    String name = sc.next();

                    System.out.println("введіть прізвище батька:");
                    while (sc.hasNextInt()) {
                        System.out.print("не можна назвати людину номером, спробуй ще раз:\n");
                        sc.next();
                    }
                    String surname = sc.next();

                    System.out.println("введіть iq батька:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 200 || intInput < 1)
                            System.out.println("введіть iq в межах від 0 до 200:");
                    } while (intInput > 200 || intInput < 1);
                    int iq = intInput;

                    System.out.println("Введіть рік народження батька:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число від 1900 до 2200, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 2200 || intInput < 1900) System.out.println("треба ввести число від 1900 до 2200, спробуй ще раз:");
                    } while (intInput > 2200 || intInput < 1900);
                    int year = intInput;

                    System.out.println("Введіть місяць народження батька:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число від 1 до 12, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 12 || intInput < 1) System.out.println("треба ввести число від 1 до 12, спробуй ще раз:");
                    } while (intInput > 12 || intInput < 1);
                    int month = intInput;

                    System.out.println("Введіть день народження батька:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число від 1 до 31, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 31 || intInput < 1) System.out.println("треба ввести число від 1 до 31, спробуй ще раз:");
                    } while (intInput > 31 || intInput < 1);
                    int day = intInput;

                    LocalDate birthDate= LocalDate.of(year, month, day);
                    Man father = new Man(name, surname, iq, birthDate);

                    //mfnb---------------------------------------------------------------
                    System.out.println("введіть ім'я мітері:");
                    while (sc.hasNextInt()) {
                        System.out.print("не можна назвати людину номером, спробуй ще раз:\n");
                        sc.next();
                    }
                    name = sc.next();

                    System.out.println("введіть прізвище матері:");
                    while (sc.hasNextInt()) {
                        System.out.print("не можна назвати людину номером, спробуй ще раз:\n");
                        sc.next();
                    }
                    surname = sc.next();

                    System.out.println("введіть iq матері:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 200 || intInput < 1)
                            System.out.println("введіть iq в межах від 0 до 200:");
                    } while (intInput > 200 || intInput < 1);
                    iq = intInput;

                    System.out.println("Введіть рік народження матері:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число від 1900 до 2200, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 2200 || intInput < 1900) System.out.println("треба ввести число від 1900 до 2200, спробуй ще раз:");
                    } while (intInput > 2200 || intInput < 1900);
                    year = intInput;

                    System.out.println("Введіть місяць народження матері:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число від 1 до 12, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 12 || intInput < 1) System.out.println("треба ввести число від 1 до 12, спробуй ще раз:");
                    } while (intInput > 12 || intInput < 1);
                    month = intInput;

                    System.out.println("Введіть день народження матері:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("треба ввести число від 1 до 31, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 31 || intInput < 1) System.out.println("треба ввести число від 1 до 31, спробуй ще раз:");
                    } while (intInput > 31 || intInput < 1);
                    day = intInput;

                    birthDate= LocalDate.of(year, month, day);
                    Woman mother = new Woman(name, surname, iq, birthDate);

                    fc.createNewFamily(father, mother);
                    break;

                }

                //----------------------------------------------------------------
                case 7:
                {
                    System.out.println("введіть число:");
                    while (!sc.hasNextInt()) {
                    System.out.print("потрібно ввести число, спробуй ще раз:\n");
                    sc.next();
                    }
                    intInput = sc.nextInt();
                    if (intInput <= fc.count() && intInput > 0) {
                        fc.deleteFamilyByIndex(intInput);
                        System.out.println("Сім'я №" + intInput + " видалена!");
                    } else System.out.println("немає такої сім'ї");
                    break;
                }


                //----------------------------------------------------------------
                case 8:
                {
                    System.out.println("Введіть підкоманду команди 8:");
                    do {
                        while (!sc.hasNextInt()) {
                            System.out.print("такої команди немає, спробуй ще раз:\n");
                            sc.next();
                        }
                        intInput = sc.nextInt();
                        if (intInput > 3 || intInput < 1) System.out.println("такої команди немає, спробуй ще раз:");
                    } while (intInput > 3 || intInput < 1);

                    switch (intInput) {
                        case 1:
                            System.out.println("Введіть номер сім'ї:");
                            do {
                                while (!sc.hasNextInt()) {
                                    System.out.print("треба ввести номер сім'ї, спробуй ще раз:\n");
                                    sc.next();
                                }
                                intInput = sc.nextInt();
                                if (intInput > fc.count() || intInput < 1)
                                    System.out.println("такої сім'ї немає, спробуй ще раз:");
                            } while (intInput > fc.count() || intInput < 1);

                            System.out.println("введіть ім'я якщо народиться дівчинка:");
                            while (sc.hasNextInt()) {
                                System.out.print("не можна назвати дитину номером, спробуй ще раз:\n");
                                sc.next();
                            }

                            String femalename = sc.next();
                            System.out.println("введіть ім'я якщо народиться хлопчик:");
                            while (sc.hasNextInt()) {
                                System.out.print("не можна назвати дитину номером, спробуй ще раз:\n");
                                sc.next();
                            }
                            String malename = sc.next();
                            fc.bornChild(dao.getFamilyByIndex(intInput), malename, femalename);
                            break;

                        case 2:
                            System.out.println("Введіть номер сім'ї:");
                            do {
                                while (!sc.hasNextInt()) {
                                    System.out.print("треба ввести номер сім'ї, спробуй ще раз:\n");
                                    sc.next();
                                }
                                intInput = sc.nextInt();
                                if (intInput > fc.count() || intInput < 1)
                                    System.out.println("такої сім'ї немає, спробуй ще раз:");
                            } while (intInput > fc.count() || intInput < 1);
                            int familyn = intInput;

                            System.out.println("введіть стать (boy/girl):");
                            do {
                                while (sc.hasNextInt()) {
                                    System.out.print("треба ввести boy або girl, спробуй ще раз:\n");
                                    sc.next();
                                }
                                stringInput = sc.next();
                                if (!stringInput.matches("boy") && !stringInput.matches("girl"))
                                    System.out.println("треба ввести boy або girl, спробуй ще раз:");
                            } while (!stringInput.matches("boy") && !stringInput.matches("girl"));
                            String gender = stringInput;

                            System.out.println("введіть ім'я:");
                            while (sc.hasNextInt()) {
                                System.out.print("не можна назвати дитину номером, спробуй ще раз:\n");
                                sc.next();
                            }
                            String name = sc.next();

                            System.out.println("введіть прізвище:");
                            while (sc.hasNextInt()) {
                                System.out.print("не можна назвати дитину номером, спробуй ще раз:\n");
                                sc.next();
                            }
                            String surname = sc.next();

                            System.out.println("введіть iq:");
                            do {
                                while (!sc.hasNextInt()) {
                                    System.out.print("треба ввести число, спробуй ще раз:\n");
                                    sc.next();
                                }
                                intInput = sc.nextInt();
                                if (intInput > 200 || intInput < 1)
                                    System.out.println("введіть iq в межах від 0 до 200:");
                            } while (intInput > 200 || intInput < 1);
                            int iq = intInput;

                            System.out.println("введіть дату народження у форматі dd/MM/yyyy:");
                            do{
                                stringInput = sc.next();
                                if(!stringInput.matches("^\\d{2}/\\d{2}/\\d{4}$")) System.out.print("треба ввести дату в форматі dd/MM/yyyy, спробуй ще раз:\n");
                            }while (!stringInput.matches("^\\d{2}/\\d{2}/\\d{4}$"));

                            LocalDate birthDate= LocalDate.of(
                                    Integer.parseInt(stringInput.split("/")[2]),
                                    Integer.parseInt(stringInput.split("/")[1]),
                                    Integer.parseInt(stringInput.split("/")[0]));
                            if (gender.matches("boy")) fc.adoptChild(dao.getFamilyByIndex(familyn), new Man(name, surname, iq, birthDate));
                            else fc.adoptChild(dao.getFamilyByIndex(familyn), new Woman(name, surname, iq, birthDate));
                            break;

                        case 3:
                            break;
                    }
                    break;
                }

                //----------------------------------------------------------------
                case 9:
                {
                    System.out.println("введіть число:");
                    while (!sc.hasNextInt()) {
                        System.out.print("потрібно ввести число, спробуй ще раз:\n");
                        sc.next();
                    }
                    intInput = sc.nextInt();
                    fc.deleteAllChildrenOlderThan(intInput);
                    System.out.println("Видалені всі діти старше " + intInput);
                    break;
                }

                //----------------------------------------------------------------
                case 10:
                {
                    fc.unloadData();
                    break;
                }

                case 11:
                {
                    fc.loadData(dao.getAllFamilies());
                    break;
                }
            }
        }
    }
}
