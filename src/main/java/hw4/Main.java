package hw4;

public class Main {
    public static void main(String[] args) {
        String[][] schedule1 = {{"monday","baseball"},{"friday","camping"}};
        String[] habits1 = {"sleeps under bed","plays with kids"};
        String[] habits2 = {"steals fish"};

        //family 1
        Human father1 = new Human("Sergei", "Ignishenko", 1960, 90, schedule1);
        Human mother1 = new Human("Oksana", "Ignishenko", 1962, 130, schedule1);
        Human child11 = new Human("Petro", "Ignishenko", 2001, 100, schedule1);
        Human child12 = new Human("Maria", "Ignishenko", 1993, 110, schedule1);
        Pet pet1 = new Pet("dog", "Charlie", 7, 30, habits1);

        Family family1 = new Family(father1, mother1);
        family1.setPet(pet1);
        family1.addChild(child11);
        family1.addChild(child12);

        //family 2
        Human father2 = new Human("Ihor", "Shevchenko", 1975, 110, schedule1);
        Human mother2 = new Human("Yulia", "Shevchenko", 1973, 100, schedule1);
        Human child21 = new Human("Vlad", "Shevchenko", 2001, 90, schedule1);
        Human child22 = new Human("Sophia", "Shevchenko", 2004, 130, schedule1);
        Human child23 = new Human("Ivan", "Shevchenko", 2005, 95, schedule1);
        Human child24 = new Human("Oleg", "Shevchenko", 1990, 100, schedule1);
        Pet pet2 = new Pet("cat", "Luna", 2, 80, habits2);

        Family family2 = new Family(father2, mother2);
        family2.setPet(pet2);
        family2.addChild(child21);
        family2.addChild(child22);
        family2.addChild(child23);
        family2.addChild(child24);

        //families toString
        System.out.println(family1.toString());
        System.out.println(family2.toString());

        family2.deleteChild(3); //Ivan
        System.out.println(family2.toString());

        //child all methods
        child21.describePet(); //cat хитрий
        child11.describePet(); //dog не хитрий
        child11.greetPet();
        System.out.println(child11.toString());
        System.out.println();

        //pet all methods
        pet1.respond();
        pet1.eat();
        pet1.foul();
        System.out.println(pet1.toString());
    }
}
