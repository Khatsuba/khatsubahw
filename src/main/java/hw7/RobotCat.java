package hw7;
import java.util.Set;

public class RobotCat extends Pet {
    //-------------------------------------------------------------------------
    //constructors
    RobotCat(EnumSpecies species, String nickname) {
        super(EnumSpecies.RobotCat,nickname);
    }
    RobotCat(String nickname, int age, int tricklevel, Set<String> habits) {
        super(EnumSpecies.RobotCat,nickname,age,tricklevel,habits);
    }
    RobotCat() {
        super();
    }
    //-------------------------------------------------------------------------
    //methods
    void respond(){
        System.out.printf("Привіт, хазяїн. Я - робокіт %s. Очікую команди!\n", this.nickname);
    }
}

