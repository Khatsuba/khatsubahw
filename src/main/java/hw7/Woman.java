package hw7;

import java.util.Map;

public class Woman extends Human {
    Woman(String name, String surname, int year) {
        super(name,surname,year);
    }
    Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }
    Woman() {
        super();
    }
    void greetPet(Pet pet){
        if (getFamily().getPets().contains(pet)){
            System.out.printf("Привіт, %s (я жінка)\n", pet.getNickname());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());
    }
    void makeup() {
        System.out.println("Чарівниця готується зачарувати світ: makeup в процесі.");
    }
}
