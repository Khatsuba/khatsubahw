package hw7;

import java.util.Map;

public class Man extends Human {
    Man(String name, String surname, int year) {
        super(name,surname,year);
    }
    Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }
    Man() {
        super();
    }

    void greetPet(Pet pet){
        if (getFamily().getPets().contains(pet)){
            System.out.printf("Привіт, %s (я чоловік)\n", pet.getNickname());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());
    }

    void fixCar() {
        System.out.println("Майстер автомеханік у дії: fixCar в процесі.");
    }
}
