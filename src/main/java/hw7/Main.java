package hw7;
import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule1 = new HashMap();
        schedule1.put(DayOfWeek.Monday, "baseball");
        schedule1.put(DayOfWeek.Tuesday, "picnic");
        schedule1.put(DayOfWeek.Wednesday, "workout");
        schedule1.put(DayOfWeek.Thursday, "visit grandma");
        schedule1.put(DayOfWeek.Friday, "shopping");
        schedule1.put(DayOfWeek.Saturday, "yoga");
        schedule1.put(DayOfWeek.Sunday, "baseball");

        Set<String> habits1 = new HashSet<>();
        habits1.add("sleeps under bed");
        habits1.add("plays with kids");

        Set<String> habits2 = new HashSet<>();
        habits1.add("steals fish");

        //family 1
        Man father1 = new Man("Sergei", "Ignishenko", 1960, 90, schedule1);
        Woman mother1 = new Woman("Oksana", "Ignishenko", 1962, 130, schedule1);
        Man child11 = new Man("Petro", "Ignishenko", 2001, 100, schedule1);
        Woman child12 = new Woman("Maria", "Ignishenko", 1993, 110, schedule1);
        Dog pet1 = new Dog("Charlie", 7, 30, habits1);

        Family family1 = new Family(father1, mother1);
        family1.addPet(pet1);
        family1.addChild(child11);
        family1.addChild(child12);

        //family 2
        Man father2 = new Man("Ihor", "Shevchenko", 1975, 110, schedule1);
        Woman mother2 = new Woman("Yulia", "Shevchenko", 1973, 100, schedule1);
        Man child21 = new Man("Vlad", "Shevchenko", 2001, 90, schedule1);
        Woman child22 = new Woman("Sophia", "Shevchenko", 2004, 130, schedule1);
        Man child23 = new Man("Ivan", "Shevchenko", 2005, 95, schedule1);
        Man child24 = new Man("Oleg", "Shevchenko", 1990, 100, schedule1);
        DomesticCat pet2 = new DomesticCat("Luna", 2, 80, habits2);
        DomesticCat pet3 = new DomesticCat("Sima", 4, 20, habits2);

        Family family2 = new Family(father2, mother2);
        family2.addPet(pet2);
        family2.addPet(pet3);
        family2.addChild(child21);
        family2.addChild(child22);
        family2.addChild(child23);
        family2.addChild(child24);

        //families toString-----------------------------

        //System.out.println(family1.toString());
        //System.out.println(family2.toString());
        //family2.deleteChild(3); //Ivan
        //family2.deleteChild(child22); //Sophia
        System.out.println(family2.toString());

        //child all methods------------------------------

        child11.describePet(pet1); //dog не хитрий
        child11.describePet(pet2); //cat не моя тварина
        child21.describePet(pet2); //cat хитрий
        child21.describePet(pet3); //cat не хитрий

        System.out.println();

        child12.greetPet(pet2); // не моя тварина
        child21.greetPet(pet2); // я чоловік
        child22.greetPet(pet3); // я жінка
        //System.out.println(child11.toString());
        //System.out.println();

        //pet all methods---------------------------------

        //pet1.respond();
        //pet1.eat();
        //pet1.foul();
        //System.out.println(pet1.toString());
        //System.out.println();

        //man-woman-methods-------------------------------

        //child11.fixCar();
        //child22.makeup();

        //garbage collector------------------------------
        //for(int i=0; i<1000000; i++){
        //    Human newHuman = new Human("name", "surname", 2000, 100, schedule1);
        //}
    }
}
