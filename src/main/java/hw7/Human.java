package hw7;

import java.util.Map;

public class Human {
    //characteristics
    private String name;                      //+++
    private String surname;                   //+++
    private int year;                         //+++
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;
    //-------------------------------------------------------------------------
    //constructors
    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    Human(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    Human() {

    }
    //-------------------------------------------------------------------------
    //methods
    void greetPet(Pet pet){
        if (family.getPets().contains(pet)){
            System.out.printf("Привіт, %s \n", pet.getNickname());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());

    }
    void describePet(Pet pet){
        if (family.getPets().contains(pet)){
            if (pet.getTricklevel()>50){
                System.out.printf("У мене є %s, %s, їй %s років, він дуже хитрий\n", pet.getSpecies(), pet.getNickname(), pet.getAge());
            }
            else System.out.printf("У мене є %s, %s, їй %s років, він майже не хитрий\n", pet.getSpecies(), pet.getNickname(), pet.getAge());
        }
        else System.out.printf("%s на ім'я %s - не моя тварина\n", pet.getSpecies(), pet.getNickname());

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Human{name='%s', surname='%s', year=%s, iq=%s, schedule=[ [ Monday,%s ] [ Tuesday,%s ] [ Wednesday,%s ] [ Thursday,%s ] [ Friday,%s ] [ Saturday,%s ] [ Sunday,%s ]",
                name,
                surname,
                year,
                iq,
                getSchedule().get(DayOfWeek.Monday),
                getSchedule().get(DayOfWeek.Tuesday),
                getSchedule().get(DayOfWeek.Wednesday),
                getSchedule().get(DayOfWeek.Thursday),
                getSchedule().get(DayOfWeek.Friday),
                getSchedule().get(DayOfWeek.Saturday),
                getSchedule().get(DayOfWeek.Sunday)
        ));
        sb.append(" ]}");
        return sb.toString();
    }
    //--------------------------------------------------------------------------------------------------
    //get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    //equals
    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human human = (Human)obj;
        return name == human.name && surname == human.surname && year == human.year;
    }

    //finalize
    protected void finalize() {
        System.out.println(this);
    }
}
