package hw7;

public enum EnumSpecies {
    RobotCat,
    DomesticCat,
    Dog,
    Fish,
    UNKNOWN
}